import json
from google.cloud import firestore
import pygeohash
from datetime import datetime
import random

from fs_utils import set_document
from summary import get_summary
import os

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'C:/dev/bin/godKey_developer.json'

collection_alert = 'alerts_log'


def send_data(latitude, longitude, type_value):
    document_to_send = {
        'geopoint': firestore.GeoPoint(latitude, longitude),
        'geohash': pygeohash.encode(latitude, longitude),
        'geohash5': pygeohash.encode(latitude, longitude, precision=5),
        'stationID': "TEST",
        'variable': "TEST",
        'type_alert': "TEST",
        'value': round(random.random()*100, 3),
        'measuredAt': datetime.timestamp(datetime.utcnow()),
        'type': type_value
    }
    document_to_send['summary'] = get_summary(document_to_send)
    set_document(
        collection_alert, f'{document_to_send["variable"]}.{document_to_send["stationID"]}.{document_to_send["measuredAt"]}', document_to_send)

    return document_to_send


latitude, longitude = (-23, -46)
type_value = [
    'intensity_rain',
    'accumulated_rain_volume',
    'wind_speed',
    'air_humidity',
    'temperature'
]

for alert in type_value:
    send_data(latitude, longitude, alert)
