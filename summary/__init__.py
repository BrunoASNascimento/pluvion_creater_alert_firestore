def get_summary(data):
    type_name = data['type']
    variable = data['variable']

    if type_name == 'accumulated_rain_volume':
        info = 'Volume de chuva acima do normal'
    if type_name == 'air_humidity':
        info = 'Umidade do ar abaixo do ideal'
    if type_name == 'intensity_rain':
        info = 'Intensidade de chuva acima do normal'
    if (type_name == 'temperature'):
        info = 'Temperaturas abaixo do ideal'
    if type_name == 'wind_speed':
        info = 'Rajadas de vento acontecendo'

    return info
